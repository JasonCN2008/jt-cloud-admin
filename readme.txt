京淘-权限管理子系统
----------------------------
第一步:克隆jt-cloud-admin
git clone https://gitee.com/JasonCN2008/jt-cloud-admin.git

第二步:通过idea打开jt-cloud-admin
1)配置maven
2)配置编译环境
3)配置编码方式

第三步:初始化数据库中的数据
1)登录mysql
2)执行指令 source d:/jt-cloud-admin.sql

第四步:启动服务
1)启动nacos(将来要修改连接nacos的ip和端口)
2)启动jt-auth认证服务
3)启动jt-gateway网关服务
4)启动jt-system 系统资源服务(用户,日志,...)
5)启动jt-ui工程
5.1)在idea中右键jt-ui工程,打开终端
5.2)npm init  (一直回车)
5.3)npm install --save-dev lite-server (安装lite-server)
5.4)npm run dev (启动服务)
5.5)登录账号(tony/123456,admin/123456)