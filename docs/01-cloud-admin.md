## 系统设计说明



* 背景

任何一个后端业务系统都会有一个权限管理子系统，这个子系统主要负责对用户进行身份认证、授权、鉴权，合法用户在有权限的情况下才可以访问我们系统中的资源。防止恶意用户对资源进行破坏。

* 名字描述

京淘供应链系统中的权限管理子系统。(前台电商负责销售，后台供应链提供商品和配送)

达人购权限管理子系统。

动吧旅游系统中权限管理子系统。

达内智库平台权限管理子系统

沙龙教育权限管理子系统

.......

* 核心业务描述

菜单管理、角色管理、部门管理、用户管理、日志管理、公告管理,......



## 菜单模块设计

### 业务描述

菜单是资源的外在表现形式，我们所说的资源管理，一般都是通过操作菜单进行实现。

### 原型设计

- 菜单列表页面：

![image-20220321100739743](01-cloud-admin.assets/image-20220321100739743.png)



- 菜单添加页面

当在菜单列表页面，点击添加按钮时，加载添加页面，例如：

![image-20220321110142451](01-cloud-admin.assets/image-20220321110142451.png)

点击上级菜单时，可选择当前要创建的新菜单对应的上级菜单，例如：

![image-20220321110232789](01-cloud-admin.assets/image-20220321110232789.png)

- 菜单编辑页面：

我们在菜单列表页面，选中一条记录，然后点击修改按钮，此时会加载菜单编辑页面，然后从数据库异步去查询当前菜单id对应的菜单数据，并将菜单数据呈现在页面。

![image-20220321105931816](01-cloud-admin.assets/image-20220321105931816.png)

在编辑页面中点击上级菜单时，呈现一个菜单树，树中内容为我们底层基于ajax方式异步加载的菜单数据，例如：

![image-20220321110101052](01-cloud-admin.assets/image-20220321110101052.png)



### 表(Menu)设计

在设计类似数据有层次结构的表时，表中字段一般会添加一个parentId，用于指向上一级的数据信息。同时一般还有有一些sort字段，用于做排序操作。这里的菜单表是资源的一种外在表现形式，访问资源一般需要有一定权限，所以在设计表时我们通常要给定访问这个资源需要什么权限(permission)

![image-20220321103102389](01-cloud-admin.assets/image-20220321103102389.png)



### 逻辑对象设计



- Pojo （SysMenu)
- Dao (SysMenuDao,SysMenuMapper.xml)
- Service(SysMenuService)
- Controller (SysMenuController)



### 业务测试设计



```
### 查询所有菜单信息
GET http://localhost:8091/menu/

### 基于菜单查询指定菜单信息
GET http://localhost:8091/menu/25

### 保存菜单信息
POST http://localhost:8091/menu/
Content-Type: application/json

{
  "name": "字典管理",
  "parentId": 8,
  "url": "/page/sys/zidian_list",
  "type": 1,
  "sort": 19,
  "remark": "zidian ...",
  "permission": "sys:zidian:list"
}

### 保存菜单信息
PUT http://localhost:8091/menu/
Content-Type: application/json

{
  "id": "157",
  "name": "字典管理",
  "parentId": 8,
  "url": "/page/sys/zidian_list",
  "type": 1,
  "sort": 19,
  "remark": "基础数据的管理，例如城市",
  "permission": "sys:zidian:list"
}

### 查询菜单树(id,name,parentId)
GET http://localhost:8091/menu/treeNodes

```



## 角色模块设计



### 业务描述

角色是作用于用户的一个对象，我们系统中的用户会按照其角色的不同分配不同的权限。在一个权限管理系统中，通常会将资源(菜单)的访问权限分配给角色，然后将角色赋予用户。用户拥有了这个角色就相当于拥有了这个角色下对应资源(菜单)的访问权限。

### 原型设计



* 列表页面

当点击菜单中的角色管理时，会分页查询角色信息，然后呈现在列表页面。

![image-20220321140731769](01-cloud-admin.assets/image-20220321140731769.png)

* 添加页面

在角色列表页面点击添加按钮时，加载角色添加页面，当角色页面加载完成可以发送异步请求，从服务端获取菜单信息，并以树结构形式呈现在角色添加页面上。

![image-20220321141711444](01-cloud-admin.assets/image-20220321141711444.png)

在添加页面中输入角色信息，并为角色分配菜单的访问权限，然后点击添加，将数据异步提交给服务端，服务端

获取到数据后，会将角色自身数据持久到角色表(sys_roles)，角色菜单关系数据持久化到角色菜单关系表(sys_role_menus)。

* 角色编辑页面

当我们点击角色列表页面中某一行角色的修改按钮时，首先要呈现角色编辑页面，并且异步加载菜单树，最后还要基于角色id异步查询角色信息以及角色对应的菜单信息，并更新在页面上。

![image-20220321144230023](01-cloud-admin.assets/image-20220321144230023.png)

当我们在页面上修改完角色数据，点击保存按钮时，将页面数据(角色自身信息，角色菜单关系数据)提交到服务端，服务端收到数据后会将数据持久化到数据库，具体过程是先更新角色自身信息，然后删除角色菜单原有关系数据，再添加新的关系数据。



### 表(Role)设计

* 角色表设计如下：

![image-20220321141443021](01-cloud-admin.assets/image-20220321141443021.png)

思考：

1)角色表与菜单表有什么关系？（many2many）

2)角色表与菜单表之间的关系有谁维护？（中间表~也称之为关系表）



* 角色菜单关系表：

![image-20220321162540035](01-cloud-admin.assets/image-20220321162540035.png)



### 逻辑对象设计

* Pojo (SysRole)
* Dao (SysRoleDao,SysRoleMenuDao,SysRoleMapper.xml,SysRoleMenuMapper.xml)
* Service(SysRoleService,SysRoleServiceImpl)
* Controller(SysRoleController)



### 测试逻辑设计



```
## 分页查询所有角色信息
GET http://localhost:8091/role/?pageCurrent=1&name=工程师

### 添加角色信息

POST http://localhost:8091/role/
Content-Type: application/json

{
 "name": "程序员鼓励师",
 "remark": "精神鼓励,...",
 "menuIds": [115,116]
}

### 基于角色id查询角色权限信息
GET http://localhost:8091/role/58

### 基于角色id更新角色信息

PUT http://localhost:8091/role/
Content-Type: application/json

{
  "id": 58,
  "name": "程序员鼓励师",
  "remark": "精神鼓励,...",
  "menuIds": [115,117,118]
}

```



## 部门模块设计

此模块结构上类似菜单模块，自己进行查阅分析。



## 用户模块设计

### 业务描述

这里的用户为我们的系统登陆用户，此用户可以有多个角色，当然同一个角色可以授予不同的用户，所以用户和角色之间是一种多对多的关系。

### 原型设计

* 用户列表页面

我们在主页面点击用户管理时，首先加载并呈现用户列表页面，然后客户端底层会发起异步ajax请求，分页查询用户信息、用户对应的部门信息，并在页面上进行局部更新，刷新页面。

![image-20220321161901678](01-cloud-admin.assets/image-20220321161901678.png)

在当前页面中可以点击分页按钮查询上一页、下一页用户信息，点击启用、禁用按钮可以修改用户的状态。

* 添加用户页面

在用户列表页面，点击添加按钮时，先加载用户添加页面，并进行呈现，然后向服务端发送异步ajax请求，加载数据库中角色信息，并呈现在此页面中，例如：

![image-20220321163905549](01-cloud-admin.assets/image-20220321163905549.png)

我们可以在此页面点击所属部门时，向服务端发送异步请求，查询部门信息，并以树结构方式进行呈现，例如：

![image-20220321164056305](01-cloud-admin.assets/image-20220321164056305.png)

在用户添加页面中写好所有数据后，点击save按钮，向服务端发送异步ajax请求，将用户信息，用户角色关系数据持久化到服务端数据库中。



* 用户修改页面

我们在用户列表页面，选中一个用户记录，然后点击修改按钮，此时加载修改页面，并且底层向服务端发送ajax异步请求，通过id获取用户信息，用户对应的部门信息，用户对应的角色id，最后将获取的结果呈现在页面上，例如：

![image-20220321165310498](01-cloud-admin.assets/image-20220321165310498.png)

我们在编辑页面修改用户信息，用户对应的部门，用户对应的角色信息，修改好以后，点击save按钮，此时向服务端发送ajax异步请求，将这些数据传递到服务端，服务端收到数据后，将数据持久化到数据库表中。其过程是先更新用户自身信息，然后删除用户和角色关系数据并添加新的用户角色关系数据。

### 表(User)设计

* 用户表

![image-20220321162254148](01-cloud-admin.assets/image-20220321162254148.png)

* 用户角色关系表

![image-20220321162454231](01-cloud-admin.assets/image-20220321162454231.png)



###  逻辑对象设计



* Pojo (SysUser)
* Dao (SysUserDao,SysUserRoleDao,SysUserMapper.xml,SysUserRoleMapper.xml)
* Service(SysUserService,SysUserServiceImpl)
* Controller (SysUserController)



### 测试逻辑设计



```
### 分页查询用户信息
GET http://localhost:8091/user/?pageCurrent=1

### 禁用用户 (携带令牌去访问)
PATCH http://localhost:8091/user/1/0

### 添加用户
POST http://localhost:8091/user/
Content-Type: application/json

{
  "username": "t-user1",
  "password": "123456",
  "mobile": "13911112345",
  "email": "tt1@t.com",
  "deptId": 8,
  "valid": 1,
  "roleIds": [48,57]
}

### 基于用户id查询用户信息
GET http://localhost:8091/user/34

### 修改用户
PUT http://localhost:8091/user/
Content-Type: application/json

{
  "id": 34,
  "username": "t-user1",
  "password": "123456",
  "mobile": "13911112345",
  "email": "t1@t.com",
  "deptId": 8,
  "valid": 1,
  "roleIds": [48,59]
}

```


## 用户行为日志设计

这里的设计与我们学习单点登陆时的设计是类似的。