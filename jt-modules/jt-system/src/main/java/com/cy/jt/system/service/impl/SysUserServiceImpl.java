package com.cy.jt.system.service.impl;

import com.cy.jt.common.exception.ServiceException;
import com.cy.jt.system.dao.SysMenuDao;
import com.cy.jt.system.dao.SysRoleMenuDao;
import com.cy.jt.system.dao.SysUserDao;
import com.cy.jt.system.dao.SysUserRoleDao;
import com.cy.jt.system.domain.SysUser;
import com.cy.jt.system.service.SysUserService;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.List;
import java.util.UUID;

@Transactional(rollbackFor = RuntimeException.class,
        isolation = Isolation.READ_COMMITTED)
@Service
public class SysUserServiceImpl implements SysUserService {
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    private SysUserRoleDao sysUserRoleDao;

    @Autowired
    private SysRoleMenuDao sysRoleMenuDao;

    @Autowired
    private SysMenuDao sysMenuDao;

    /**
     * 基于用户名查找用户信息
     * @param username
     * @return
     */
    @Override
    public SysUser selectUserByUsername(String username){
        SysUser sysUser=sysUserDao.selectUserByUsername(username);
        return sysUser;
    }
    public List<String> selectUserPermissions(Integer userId){
        List<Integer> roleIds=sysUserRoleDao.selectRoleIdsByUserId(userId);
        List<Integer> menuIds=sysRoleMenuDao.selectMenuIdsByRoleId(roleIds);
        List<String> permissions=sysMenuDao.selectPermissions(menuIds);
        System.out.println("permissions="+permissions);
        return permissions;
    }
    public int updateUser(SysUser entity){
        //开启事务
        //1.参数校验(省略)
        //2.更新用户自身信息
        int rows=sysUserDao.updateUser(entity);
        if(rows==0)
            throw new ServiceException("记录可能已经不存在");
        //3.更新用户和角色关系数据
        sysUserRoleDao.deleteByUserId(entity.getId());
        sysUserRoleDao.insertUserRoles(entity.getId(),
                entity.getRoleIds());
        return rows;
    }

    public SysUser selectById(Integer id){
        SysUser user=sysUserDao.selectById(id);
        if(user==null)
            throw new ServiceException("此用户可能已经不存在");
        List<Integer> roleIds=sysUserRoleDao.selectRoleIdsByUserId(id);
        user.setRoleIds(roleIds);
        return user;
    }


    public int insertUser(SysUser entity){
        //对密码进行假面
        String hashedPassword=
                new BCryptPasswordEncoder().encode(entity.getPassword());
        entity.setPassword(hashedPassword);
        int rows=sysUserDao.insertUser(entity);
        sysUserRoleDao.insertUserRoles(
                entity.getId(),entity.getRoleIds());
        return rows;
    }

    public int validById(Integer id,Integer valid){
        int rows=sysUserDao.validById(id,valid,null);
        if(rows==0)throw new ServiceException("记录可能已经不存在");
        return rows;
    }


    public List<SysUser> selectUsers(SysUser sysUser){
        return sysUserDao.selectUsers(sysUser);
    }

}
