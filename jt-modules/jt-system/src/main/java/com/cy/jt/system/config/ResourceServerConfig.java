package com.cy.jt.system.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.access.AccessDeniedHandler;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    /**
     * 路由安全认证配置
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler());
        http.authorizeRequests().anyRequest().permitAll();
//      http.authorizeRequests()
//                .antMatchers("/user/**")
//                .authenticated();
    }
    //没有权限时执行此处理器方法
    public AccessDeniedHandler accessDeniedHandler() {
        return (request, response, e) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("state", HttpServletResponse.SC_FORBIDDEN);//SC_FORBIDDEN的值是403
            map.put("message", "没有访问权限,请联系管理员");
            //1设置响应数据的编码
            response.setCharacterEncoding("utf-8");
            //2告诉浏览器响应数据的内容类型以及编码
            response.setContentType("application/json;charset=utf-8");
            //3获取输出流对象
            PrintWriter out=response.getWriter();
            //4 输出数据
            String result= new ObjectMapper().writeValueAsString(map);
            out.println(result);
            out.flush();
        };
    }

}
