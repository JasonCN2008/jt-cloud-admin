package com.cy.jt.system.web.controller;

import com.cy.jt.common.domain.JsonResult;
import com.cy.jt.common.dto.UserDto;
import com.cy.jt.system.domain.SysUser;
import com.cy.jt.system.service.SysUserService;
import com.cy.jt.system.web.util.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/user/")
@RestController
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @GetMapping("auth/{username}")
    public UserDto selectUserByUsername(@PathVariable  String username){
        SysUser user =sysUserService.selectUserByUsername(username);
        UserDto userDto=new UserDto();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        return userDto;
    }

    @GetMapping("perm/{id}")
    public List<String> selectUserPermissions(@PathVariable  Integer id){
        List<String> permissions =sysUserService.selectUserPermissions(id);
        return permissions;
    }


    @GetMapping
    public JsonResult doSelectUsers(SysUser entity){
        return new JsonResult(
                PageUtils.startPage().doSelectPageInfo(()->{
                    sysUserService.selectUsers(entity);
                }));
    }
    @PostMapping
    public JsonResult doInsertUser(@RequestBody SysUser entity){
        sysUserService.insertUser(entity);
        return new JsonResult("save ok");
    }

    @GetMapping("{id}")
    public JsonResult doSelectById(@PathVariable Integer id){
        return new JsonResult(sysUserService.selectById(id));
    }


    @PutMapping
    public JsonResult doUpdateUser(@RequestBody SysUser entity){
        sysUserService.updateUser(entity);
        return new JsonResult("update ok");
    }

    @PreAuthorize("hasAuthority('sys:user:update')")
    @PatchMapping("{id}/{valid}")
//少量数据的更新可使用Patch请求,当然使用put也可以.
    public JsonResult doValidById(@PathVariable Integer id,
                                  @PathVariable Integer valid){
        sysUserService.validById(id,valid);//update
        return new JsonResult("update ok");
    }

}
