package com.cy.jt.system.dao;

import com.cy.jt.common.domain.Node;
import com.cy.jt.system.domain.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Mapper注解描述的接口，系统底层会为其产生实现类(代理类-基于JDK中的反射机制进行创建)
 * ，在代理类的内部会通过SqlSession实现与数据库的交互。
 * 注：SqlSession是MyBatis提供的一个基于SQL实现与数据库会话的接口对象。
 */

@Mapper
public interface SysMenuDao {
    /**
     * 查询所有菜单，以及菜单对应的上级菜单名称
     * @return 返回查询到的菜单信息
     */
    List<SysMenu> selectMenus();

    /**
     * 查询树节点信息，在添加或编辑菜单时，
     * 会以树结构方式呈现可选的上级菜单信息
     * @return
     */
    @Select("select id,name,parentId from sys_menus")
    List<Node>selectMenuTreeNodes();

    /**
     *定义基于菜单id查询菜单信息的方法
     */
    SysMenu selectById(Integer id);


    /**
     * 向数据库表中添加菜单信息
     * @param menu 存储了菜单信息的对象
     * @return 新增的行数
     */
    int insertMenu(SysMenu menu);
    /**
     * 基于id更新菜单信息
     * @param menu
     * @return 更新的行数
     */
    int updateMenu(SysMenu menu);

    List<String> selectPermissions(@Param("menuIds") List<Integer> menuIds);

}
