package com.cy.jt.dao;

import com.cy.jt.system.dao.SysMenuDao;
import com.cy.jt.system.domain.SysMenu;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SysMenuDaoTests {
    @Autowired
    private SysMenuDao sysMenuDao;
    @Test
    void testSelectMenus(){
        List<SysMenu> sysMenus = sysMenuDao.selectMenus();
        sysMenus.forEach(System.out::println);
    }
}
