package com.cy.jt.dao;

import com.cy.jt.system.dao.SysRoleDao;
import com.cy.jt.system.domain.SysRole;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class RoleDaoTests {

    @Autowired
    private SysRoleDao sysRoleDao;
    @Test
    void testSelectRoles(){
        SysRole sysRole=new SysRole();
        //sysRole.setName("工程师");
        List<SysRole> sysRoles = sysRoleDao.selectRoles(sysRole);
        sysRoles.forEach(System.out::println);
    }
}
